#!/bin/bash
#v1.0 dat 2020.04.22

ORIG_IFS=$IFS
readonly ORIG_IFS

TARGET=""
NAME=""
PERMISSION=""
LIST=""
DEBUG=""

printhelp() {
	echo "Update files permissions"
	echo "Usage: updateFilesPerm.sh -t -n -p [-d]  {-f | FILES...}"
	echo "Keys:"
	echo "  -t, --target=g|u                group or user target ('g' or 'u')."	
	echo "  -n, --name=NAME                 user or group name."
	echo "  -p, --permission=rw-            permission in symbolic mode."
	echo "  -f, --files=FILE_WITH_LIST      file with list of files 
                                     (each file from the new line)."
	echo "  -d, --debug                     perform a trial run with no changes made."
	echo "  --help                          this message."
	echo ""
	echo "FILES                             files (in double quotes, separate by space)."
	   
}

if [ "$1" == "--help" ]; then
	printhelp;
	exit 0;
fi

for arg in "$@"; do
	shift
	case "$arg" in
		"--target") set -- "$@" "-t" ;;
		"--name") set -- "$@" "-n" ;;
		"--permission") set -- "$@" "-p" ;;
		"--files") set -- "$@" "-f" ;;
		"--debug") set -- "$@" "-d" ;;
		*) set -- "$@" "$arg"
	esac
done

while getopts "t:n:p:f:d" opt
do
	case $opt in
		t) TARGET=$OPTARG;;
		n) NAME=$OPTARG;;
		p) PERMISSION=$OPTARG;;
		f) LIST=$OPTARG;;
		d) DEBUG=1;;
	esac
done

if [ -z "$TARGET" ] || [ -z "$NAME" ] || [ -z "$PERMISSION" ]; then
	echo "Missing one or more mandatory key"
	printhelp;
	exit 1
fi

if (( `expr length $PERMISSION` > 3 )); then
	echo "Wrong permission"
	exit 1
fi


LIST_ARR=""

IFS=$'\n'

if [ -z $LIST ]; then
	shift 6;
	[ $DEBUG ] && shift;
	LIST_ARR=("$@")
else
	if ! [ -e "$LIST" ]; then
		echo "File $LIST not exist";
		exit 2;
	fi

	if ! [ -r "$LIST" ]; then
		echo "Access to the file $LIST is denied"
		exit 3;
	fi

	while read LINE; do
		LIST_ARR+=($LINE)
	done < "$LIST"
fi


if [ $DEBUG ]; then
	for f in ${LIST_ARR[@]}; do
		echo "setfacl -R -m "$TARGET:$NAME:$PERMISSION" "$f""
		/usr/bin/setfacl --test -R -m "$TARGET:$NAME:$PERMISSION" "$f"
	done
else
	for f in ${LIST_ARR[@]}; do
		/usr/bin/setfacl -R -m "$TARGET:$NAME:$PERMISSION" "$f"
	done
fi

IFS=$ORIG_IFS

